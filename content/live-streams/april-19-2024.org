#+title: Crafting Sway with Emacs Lisp
#+subtitle: System Crafters Live - April 19, 2024
#+date: [2024-04-19 Fri]
#+video: SuVE6Ps-f0g

* News

- Hands-On Guile Scheme for Beginners, April Edition starts this Saturday!

  https://systemcrafters.net/courses/hands-on-guile-scheme-beginners/

- Sign up for the Spring Lisp Game Jam, starting May 17th!

  https://itch.io/jam/spring-lisp-game-jam-2024

  I'll be working on my own entry and streaming it on this channel!

- Join the System Crafters Craftering:

  https://codeberg.org/SystemCrafters/craftering

* Infecting Sway with Emacs

We've talked before about configuring Sway with Guile Scheme, but today we're going to try this from a different angle: driving Sway features from within Emacs.

** Goals

*** Power Swaybar with Emacs Lisp

*** Set Sway colors from Emacs theme

*** Discover other useful hacks!

** References

- https://github.com/thblt/sway.el
- https://github.com/vivien/i3blocks
- https://i3wm.org/docs/i3bar-protocol.html
